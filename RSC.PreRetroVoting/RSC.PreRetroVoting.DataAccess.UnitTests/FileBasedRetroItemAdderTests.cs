﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSC.PreRetroVoting.DataAccess.RetroItemsAdders;
using NMock;
using RSC.PreRetroVoting.DataAccess.FileSystem;

namespace RSC.PreRetroVoting.DataAccess.UnitTests
{
  [TestClass]
  public class FileBasedRetroItemAdderTests
  {
    [TestMethod]
    public void AddRetroItemTest()
    {
      const string ExpectedRetroItem = "Test Retore Item 1";

      var mockFactory = new MockFactory();
      var retroItemsFileProvider = mockFactory.CreateMock<IRetroItemsFileProvider>();
      var retroItemsFile = mockFactory.CreateMock<IRetroItemsFile>();
      var fileBasedRetroItemAdder = new FileBasedRetroItemAdder(
        retroItemsFileProvider.MockObject);

      retroItemsFileProvider.Expects.One.MethodWith(
        o => o.OpenRetroItemsFile()).WillReturn(retroItemsFile.MockObject);

      retroItemsFile.Expects.One.MethodWith(
        o => o.AddRetroItem(ExpectedRetroItem));
      retroItemsFile.Expects.One.MethodWith(
        o => o.SaveFile());
      retroItemsFile.Expects.One.MethodWith(
        o => o.Dispose());

      fileBasedRetroItemAdder.AddRetroItem(ExpectedRetroItem);

      mockFactory.VerifyAllExpectationsHaveBeenMet();
    }
  }
}
