﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSC.PreRetroVoting.DataAccess.FileSystem
{
  internal sealed class RetroItemsXmlFileProvider : IRetroItemsFileProvider
  {
    #region IRetroItemsFileProvider Members

    public IRetroItemsFile OpenRetroItemsFile()
    {
      return new RetroItemsXmlFile();
    }

    #endregion
  }
}
