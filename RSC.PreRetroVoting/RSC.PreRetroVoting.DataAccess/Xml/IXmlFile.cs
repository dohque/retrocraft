﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSC.PreRetroVoting.DataAccess.Xml
{
  internal interface IXmlFile
  {
    void AddXElement(XElement element);
  }
}
