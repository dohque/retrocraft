﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSC.PreRetroVoting.DataAccess.FileSystem;

namespace RSC.PreRetroVoting.DataAccess.RetroItemsAdders
{
  internal sealed class FileBasedRetroItemAdder : IRetroItemAdder
  {
    public FileBasedRetroItemAdder(IRetroItemsFileProvider retroItemsFileProvider)
    {
      _retroItemsFileProvider = retroItemsFileProvider;
    }

    #region IRetroItemAdder Members

    public void AddRetroItem(string itemDescription)
    {
      using (var file = _retroItemsFileProvider.OpenRetroItemsFile())
      {
        file.AddRetroItem(itemDescription);
        file.SaveFile();
      }
    }

    #endregion

    private readonly IRetroItemsFileProvider _retroItemsFileProvider;
  }
}
